import { ServiceType } from "../types"
import { YearPricing } from "./yearPricing";

// class serving prices per year per service(s). Can serve single service pricing and combined discounts
export class Pricing {
    service: ServiceType[];
    yearPrices: YearPricing[];

    constructor(service:ServiceType[], yearPrices: YearPricing[]){
        this.service = service;
        this.yearPrices = yearPrices;
    }
}