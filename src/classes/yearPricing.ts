import { ServiceType, ServiceYear } from "../types";

// class serving year and price. Used by Pricing to combine services with year pricing.
export class YearPricing {
    year: ServiceYear;
    price: number;

    constructor(year: ServiceYear, price: number){
        this.year = year;
        this.price = price;
    }
}