import { Pricing } from "./classes/pricing";
import { ServiceType, ServiceYear } from "./types";
import { ServicePricing } from "./constants";

// calculating Base price - single service entries only, no discounts included
// returns NUMBER
export function calculateBasePrice(selectedServices: ServiceType[], selectedYear: ServiceYear){
    var basePrice:number = 0;
    
    selectedServices.forEach(service => {
        // picking up only single element items from defined prices collection where service is mathing entry from input
        const foundService = ServicePricing.find(s => s.service.length === 1 && s.service[0] === service);
        // take appropriate year from found Pricing object
        const foundYear = foundService.yearPrices.find(y => y.year === selectedYear);
        basePrice += foundYear?.price ?? 0;
    });

    //console.log(basePrice);

    return basePrice;
};


// calculates price for services set with one discount provided
// returns NUMBER
export function calculatePriceWithDiscount(selectedServices:ServiceType[], selectedYear: ServiceYear, discountServices: Pricing ) { 

    //console.log('discount services: ' + discountServices.service);

    // take collection of services excluding the ones used for discount
    var servicesWithoutDiscount = selectedServices.filter(s => !discountServices.service.includes(s));
    // calculate base price for services without discounted ones
    var priceWithoutDiscountServices = calculateBasePrice(servicesWithoutDiscount, selectedYear);

    //console.log('services without discounts ' + servicesWithoutDiscount);
    
    // calculate price of entire package including discount (for specified discout)
    var priceWithDiscount = priceWithoutDiscountServices + ( discountServices?.yearPrices.find(s => s.year === selectedYear).price ?? 0);

    //console.log('Discount price: ' + priceWithDiscount);
    
    return priceWithDiscount;
}


// final prace calculation - including all discounts from package and applyting only one with the greatest price lowering. 
// Returns an object containing basePrice and finalPrice
export function calculateFinalPrice(selectedServices: ServiceType[], selectedYear: ServiceYear){
    var finalPrice:number = 0;

    var basePrice = calculateBasePrice(selectedServices, selectedYear);
    //var discountPrice = 0;
    var discountsAvailable:Pricing[] = [];

    // collecting list of available discounts from services package
    ServicePricing.filter(s => s.service.length > 1)
        .forEach(discountServices => { 
            if (discountServices.service.every(ds =>  selectedServices.includes(ds)) )
                discountsAvailable.push(discountServices);
    });
    

    var lowestDiscountPrice = basePrice;
    var lowestDiscountServices:Pricing;
    
    // calculating final price for all discounts variations and choosing one but best deal
    discountsAvailable.forEach(ds => {
        var calculatedPrice = calculatePriceWithDiscount(selectedServices, selectedYear, ds);
        if (calculatedPrice < lowestDiscountPrice){
            lowestDiscountPrice = calculatedPrice;
            // keeping dicount services used in price calculation so that we can check/log for future analysis what discounts were used most often
            lowestDiscountServices = ds; 
            // console.log(' SET calculated ' + calculatedPrice + ' lowest ' + lowestDiscountPrice);
        }
    });    

    // console.log('Base price: ' + basePrice);
    // console.log('Final price: ' + lowestDiscountPrice);
    
    return { basePrice: basePrice, finalPrice: lowestDiscountPrice };
};