// dev: I have commented out one test and added modified as I think it should behave a bit differently than in original requirement. 
// Please find more comment in tests file


import { ServiceType, ServiceYear } from "./types";
import { calculateFinalPrice } from "./calculations";


// I think user should be able to select all kind of services, even if they are dependent from non previously selected parent. 
// In my opinion that case should be handled on UI side to restrict services selection with proper message and eventually 
// calculation itself should not include cost of 'redundant' services. 
// Below is an implementatoin to meet unit tests requirements
export const updateSelectedServices = (
    previouslySelectedServices: ServiceType[],
    action: { type: "Select" | "Deselect"; service: ServiceType }
) => {

    // creating deep copy of an existing services
    var selectedServices:string[] = JSON.parse(JSON.stringify(previouslySelectedServices));

    switch (action.type) {
        case "Select": {
            // add service if not exists
            if (selectedServices.indexOf(action.service) < 0) {

                // blueRay might be added only when Video exists
                var isNotBlueRayApplicable = action.service === "BlurayPackage" && !previouslySelectedServices.includes("VideoRecording");
                // TwoDayEvent might be added when Video or Photo exists
                var isNotTwoDayEventApplicable = action.service === "TwoDayEvent" && !(previouslySelectedServices.includes("VideoRecording") || previouslySelectedServices.includes("Photography"));

                if ( !isNotTwoDayEventApplicable && !isNotBlueRayApplicable )
                    selectedServices.push(action.service);
            }
            return selectedServices;
        } 
        case "Deselect": {
            const index = selectedServices.indexOf(action.service);
            // deselect if previously selected
            if (index >= 0) {
                selectedServices.splice(index,1);
                
                // deselect dependent BlueRay when Video deselected
                var blrIndex = selectedServices.indexOf("BlurayPackage");
                if ( action.service === "VideoRecording" && blrIndex >= 0 )
                    selectedServices.splice(blrIndex,1);
                    

                // Two Day session with Photo or Viedeo
                var videoIndex = selectedServices.indexOf("VideoRecording");
                var photoIndex = selectedServices.indexOf("Photography");
                var twoDayIndex = selectedServices.indexOf("TwoDayEvent");
                
                // deselect TwoDay when no Video and Photo exist anymore
                if ( action.service === "VideoRecording" && twoDayIndex >= 0 && photoIndex < 0)
                    selectedServices.splice(twoDayIndex,1);
                if ( action.service === "Photography" && twoDayIndex >= 0 && videoIndex < 0)
                    selectedServices.splice(twoDayIndex,1);    
                    
            }
            return selectedServices;
        }
        default:
            break;
    }

    return selectedServices;
};


export const calculatePrice = (selectedServices: ServiceType[], selectedYear: ServiceYear) => 
    { return calculateFinalPrice(selectedServices,selectedYear) };
