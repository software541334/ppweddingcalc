import { Pricing } from "./classes/pricing";
import { YearPricing } from "./classes/yearPricing";

// Defined services with year pricing (including discounts - with multiple services provided). Collection can be extended by additional services (with Type updates), years, prices and discounts.
export const ServicePricing:Pricing[] = [ 
    new Pricing(["Photography"], [new YearPricing(2020,1700), new YearPricing(2021, 1800), new YearPricing(2022,1900)]) 
    , new Pricing(["VideoRecording"], [new YearPricing(2020,1700), new YearPricing(2021, 1800), new YearPricing(2022,1900)])
    , new Pricing(["VideoRecording", "Photography"], [new YearPricing(2020,2200), new YearPricing(2021, 2300), new YearPricing(2022,2500)])
    , new Pricing(["WeddingSession"], [new YearPricing(2020,600), new YearPricing(2021, 600), new YearPricing(2022,600)])
    , new Pricing(["WeddingSession","VideoRecording"], [new YearPricing(2020,1700+300), new YearPricing(2021,1800 + 300), new YearPricing(2022, 1900 + 300)])
    , new Pricing(["WeddingSession", "Photography"], [new YearPricing(2020, 1700 + 300), new YearPricing(2021, 1800 + 300), new YearPricing(2022,1900 + 0)])
    , new Pricing(["BlurayPackage"], [new YearPricing(2020,300), new YearPricing(2021, 300), new YearPricing(2022,300)])
    , new Pricing(["TwoDayEvent"], [new YearPricing(2020,400), new YearPricing(2021, 400), new YearPricing(2022,400)])
]